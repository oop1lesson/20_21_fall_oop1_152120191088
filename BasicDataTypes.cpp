#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>

using namespace std;

int main() {

    int a;
    long b;
    char c;
    float d;
    double e;

    scanf("%d %ld %c %f %lf", &a, &b, &c, &d, &e);

    printf("%d", a);
    printf("\n");
    printf("%ld", b);
    printf("\n");
    printf("%c", c);
    printf("\n");
    printf("%.3f", d);
    printf("\n");
    printf("%.9lf", e);



    return 0;
}
