#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int x, y;
    cin >> x >> y;
    int** z = new int* [x];
    for (int i = 0; i < x; i++)
    {
        int a;
        cin >> a;
        int* b = new int[a];
        for (int j = 0; j < a; j++)
        {
            int e;
            cin >> e;
            b[j] = e;
        }
        *(z + i) = b;
    }

    for (int i = 0; i < y; i++)
    {
        int v, t;
        cin >> v >> t;
        cout << z[v][t] << endl;
    }
}
